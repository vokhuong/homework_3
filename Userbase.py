import pandas as pd
import numpy as np

import pandas as pd
import numpy as np
import math

user_movie=pd.read_csv("./Data/ratings.csv")
rating_mean= user_movie.groupby(['userId'], as_index = False, sort = False).mean().rename(columns = {'rating': 'rating_mean'})[['userId', 'rating_mean']]

# Calculate mean and differ before to apply pearson metric
user_movie = pd.merge(user_movie, rating_mean, on ='userId', how ='left', sort = False)
user_movie['rating_differ'] = user_movie['rating'] - user_movie['rating_mean']

users = np.unique(user_movie['userId'])
movies = np.unique(user_movie['movieId'])



def find_rating(userId, movieId):
    user_data_append = pd.DataFrame()
    user_data_all = pd.DataFrame()
    user_dot_adj_rating_all = pd.DataFrame()


    user_data = user_movie[user_movie['userId'] == userId]
    rating_mean = user_data["rating"].mean()
    user_data = user_data.rename(columns={'rating_differ': 'rating_differ1'})
    user_data = user_data.rename(columns={'userId': 'userId1'})
    variance1 = np.sqrt(np.sum(np.square(user_data['rating_differ1']), axis=0))
    print user_data.head()

    # item_user = user_movie[user_movie['movieId'] == movie]
    relative_users =  user_movie[user_movie['movieId'] == movieId]['userId']
    print relative_users

    for relative_user in relative_users:

        relative_user_data = user_movie[user_movie['userId'] == relative_user]
        relative_user_data = relative_user_data.rename(columns={'rating_differ': 'rating_differ2'})
        relative_user_data = relative_user_data.rename(columns={'userId': 'userId2'})
        variance2 = np.sqrt(np.sum(np.square(relative_user_data['rating_differ2']), axis=0))

        print relative_user_data.head()

        merge_data = pd.merge(user_data, relative_user_data[['rating_differ2', 'movieId', 'userId2']], on='movieId',
                             how='inner', sort=False)
        merge_data['vector'] = (merge_data['rating_differ1'] * merge_data['rating_differ2'])

        merge_data = merge_data.groupby(['userId1', 'userId2'], as_index=False, sort=False).sum()

        merge_data['metric'] = merge_data['vector'] / (variance1 * variance2)
        print merge_data.head()

        user_data_all = user_data_all.append(merge_data, ignore_index=True)

    print user_data_all.head()

    user_data_all = user_data_all[user_data_all['metric'] < 1]
    user_data_all = user_data_all.sort_values(['metric'], ascending=False)
    user_data_all = user_data_all.head(30)
    user_data_all['movieId'] = movieId

    print user_data_all.head()
    user_data_append = user_data_append.append(user_data_all, ignore_index=True)

    user_data_append_movie = user_data_append[user_data_append['movieId'] == movieId]
    print user_data_append_movie.head()

    user_metric_adj_rating = pd.merge(user_movie, user_data_append_movie[['metric', 'userId2', 'userId1']], how='inner',
                                   left_on='userId', right_on='userId2', sort=False)

    print user_metric_adj_rating.head()

    user_metric_adj_rating1 = user_metric_adj_rating[user_metric_adj_rating['movieId'] == movieId]

    print user_metric_adj_rating1.head()

    # Recommend if more than 2 affect
    if len(np.unique(user_metric_adj_rating1['userId'])) >= 2:
        user_metric_adj_rating1['wt_rating'] = user_metric_adj_rating1['metric'] * user_metric_adj_rating1['rating_differ']

        user_metric_adj_rating1['similar_abs'] = user_metric_adj_rating1['metric'].abs()
        user_metric_adj_rating1 = user_metric_adj_rating1.groupby(['userId1'], as_index=False, sort=False).sum()[
            ['userId1', 'wt_rating', 'similar_abs']]
        user_metric_adj_rating1['Rating'] = (user_metric_adj_rating1['wt_rating'] / user_metric_adj_rating1[
            'similar_abs']) + rating_mean
        user_metric_adj_rating1['movieId'] = movieId
        user_metric_adj_rating1 = user_metric_adj_rating1.drop(['wt_rating', 'similar_abs'], axis=1)

        user_dot_adj_rating_all = user_dot_adj_rating_all.append(user_metric_adj_rating1, ignore_index=True)
        user_dot_adj_rating_all = user_dot_adj_rating_all.sort_values(['Rating'], ascending=False)
        return user_dot_adj_rating_all

print find_rating(320,99)