import pandas as pd
from CollaborativeFiltering import CollaborativeFiltering
import numpy as np


# user_movie = pd.read_csv("./Data/ratings.csv")
# Load data and set column for them
user_movie = pd.read_csv("./Data/u.data", sep="\t")
user_movie.columns = ["userId", "movieId", "rating", "timestamp"]

# Convert data to rating matrix with column is movieId and row is userId
rating_matrix = user_movie.pivot_table(index='userId', columns='movieId', values='rating').fillna(0).astype(int)
print rating_matrix.info()

# Convert data suitable with class CollaborativeFiltering
matrix_to_list = np.array(rating_matrix.values.tolist())
cf = CollaborativeFiltering(matrix_to_list, K=8, alpha=0.1, beta=0.001, iterations=20)
cf.train()

print cf.full_matrix()
print(cf.get_rating(1, 350))